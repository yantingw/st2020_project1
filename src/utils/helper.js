let sorting = (array) => {
    array = array.sort();
    return array;
}
let compare = (a, b) => {
    const delta = a['PM2.5'] - b['PM2.5'];

    if (delta == 0) {
        return 0;
    } else if (delta > 0) {
        return 1;
    } else {
        return -1;
    }

}

let average = (nums) => {
    var count = nums.reduce((previous, current) => current += previous);
    count = count / nums.length
    return parseFloat(count.toFixed(2));
}


module.exports = {
    sorting,
    compare,
    average
}